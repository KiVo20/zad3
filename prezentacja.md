---
author: Jakub Kurek
title: Golang
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Podstawowe informacje 

**Go (często nazywany także golang)** - wieloparadygmatowy język programowania opracowany przez pracowników firmy Google: Roberta Griesemera, Roba Pike’a oraz Kena Thompsona. Łączy w sobie łatwość pisania aplikacji charakterystyczną dla języków dynamicznych (np. Python, Lisp), jak również wydajność języków kompilowanych (np. C, C++).

![&nbsp; Gopher](./images/gopher.jpg){height=50% width=50%}

## Twórcy
![](./images/griesemer.jpg){height=42% width=42%}
![](./images/pike.jpg){height=42% width=42%}
![](./images/thompson.png){height=42% width=42%}

## Podstawy - definiowanie zmiennej

1. var nazwa_zmiennej typ = wartość (np. var test int)
2. nazwa_zmiennej := wartość (np. test := 0)
3. const nazwa_stałej_wartości

Zmienne definiowane przy pomocy := mogą być użyte tylko w funkcjach. Dodatkowo w tym przypadku kompilator sam zdecyduje jakiego typu użyć np.:
* test := 0 - zmienna typu int
* test := 0.0 - zmienna typu float64


## Definiowanie zmiennej - Przykładowy kod

### Golang

```go
package main

var globalTest int = 0
const (
    c1 string = "c1"
    c2 string = "c2"
)

func main() {
    test := 0
    test2 := 0.0
    var test3 int = 0
}
```

## Podstawy - składnia funkcji

func nazwa_funkcji(parametry) (zwracane typy) {
    ciało
}

### Golang

```go
package main

func dodajodejmij(a, b int) (int, int) {
    return a + b, a - b
}
```
